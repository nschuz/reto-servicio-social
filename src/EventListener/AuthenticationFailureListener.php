<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationFailureListener
{


    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event): void
    {
        $data = [
            'error' => array(
                "message" => 'Bad credentials, please verify that your username/password are correctly set',
                "statusCode" => Response::HTTP_BAD_REQUEST
            )
        ];
        $response = new JWTAuthenticationFailureResponse();
//        $response->setData($data);
        $response->setJson(json_encode($data));
        $event->setResponse($response);
    }
}

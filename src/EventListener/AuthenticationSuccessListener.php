<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessListener
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event): void
    {
        $data = $event->getData();
//        $user = $event->getUser();
//
//        if (!$user instanceof UserInterface) {
//            return;
//        }

//        $data['data'] = array(
//            'roles' => $user->getRoles(),
//        );

        $event->setData(array(
            "data" => $data
        ));
    }
}

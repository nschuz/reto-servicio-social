<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTAuthenticatedListener
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function onJWTAuthenticated(JWTAuthenticatedEvent $event): void
    {
//        $token = $event->getToken();
//        $payload = $event->getPayload();
//
//        $token->setAttribute('userId', $payload['username']);
    }
}

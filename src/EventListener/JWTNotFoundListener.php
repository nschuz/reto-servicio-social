<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class JWTNotFoundListener
{
    public function onJWTNotFound(JWTNotFoundEvent $event): void
    {
        $data = [
            'error' => array(
                'message' => 'Missing token',
                'statusCode' => 403
            ),
        ];

        $response = new JsonResponse($data, 403);

        $event->setResponse($response);
    }
}

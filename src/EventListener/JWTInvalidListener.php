<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\Response;

class JWTInvalidListener
{

    public function onJWTInvalid(JWTInvalidEvent $event): void
    {
        $data = [
            'error' => array(
                "message" => 'Bad credentials, please verify that your username/password are correctly set',
                "statusCode" => Response::HTTP_FORBIDDEN
            )
        ];

        $response = new JWTAuthenticationFailureResponse();
        $response->setJson(json_encode($data));
        $event->setResponse($response);
    }
}

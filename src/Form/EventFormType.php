<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Título'
                ),
                'label' => "Título "
            ])
            ->add('organizer', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Organizador'
                ),
                'label' => "Organizador "
            ])
            ->add('description', TextAreaType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Descripción'
                ),
                'label' => "Descripción "
            ])
            ->add('presale', DateTimeType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Preventa'
                ),
                'label' => "Preventa "
            ])
            ->add('price', NumberType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Precio'
                ),
                'label' => "Precio "
            ])
            ->add('percentageDiscount', NumberType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Descuento'
                ),
                'label' => "Descuento "
            ])
            ->add('scheduledAt', DateTimeType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Programado'
                ),
                'label' => "Programado "
            ])
            ->add('restrictions', TextAreaType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Restricciones'
                ),
                'label' => "Restricciones "
            ])
            ->add('estimatedDuration', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Duración estimada'
                ),
                'label' => "Duración "
            ])
            ->add('banner', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Image URL'
                ),
                'label' => "Imagen URL"
            ])
            ->add('ageLimit', NumberType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Límite de edad'
                ),
                'label' => "Límite de edad "
            ])
            ->add('location', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Location'
                ),
                'label' => "Locacion"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}

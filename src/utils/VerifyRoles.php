<?php

namespace App\utils;

use App\Service\ApiResponse;

class VerifyRoles
{
    public static function isAdmin($decodedJwtToken): bool {
        return !in_array("ROLE_ADMIN", $decodedJwtToken['roles']);
    }
}

<?php


namespace App\DataFixtures;

use App\Entity\Event;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $event = new Event();
        $event->setTitle("Concierto David Garret | Espaciio para opera.");
        $event->setDescription("Opera David Garret con mas de 10 anios de experiencia.");
        $event->setAgeLimit(3);
        $event->setBanner("https://www.loomee-tv.de/wp-content/uploads/2011/11/David-Garrett-04_2011-CMS-Source.jpg");
        $event->setEstimatedDuration("2 horas.");
        $event->setOrganizer("David Garret");
        $event->setPercentageDiscount(10);
        $event->setPrice(7000);
        $event->setPresale(new \DateTime("10/25/2023 14:30:00"));
        $event->setScheduledAt(new \DateTime("10/30/2023 14:30:00"));
        $event->setRestrictions("Espacio cerrado. No esta permitido fumar.");
        $event->setLocation("Auditorio de Monterry, Monterry Mexico");
        $event->setUserId($this->getReference('user'));
        $manager->persist($event);

        $event2 = new Event();
        $event2->setTitle("Conciero Paul Van Dyk | Musica Electronica ");
        $event2->setDescription("Unos de los padres de la musica Trance , regresa a  mexico.
        -No te pierdas un increible concierto de musica trance.
        -Vive los utimos 30 anios de carrera de paul van dyk");
        $event2->setAgeLimit(18);
        $event2->setBanner("http://www.djbios.com/wp-content/uploads/2017/03/Paul-van-Dyk-Dreamstate.jpg");
        $event2->setEstimatedDuration("2 horas.");
        $event2->setOrganizer("Paul Van Dyk");
        $event2->setPercentageDiscount(null);
        $event2->setPrice(5000);
        $event2->setPresale(new \DateTime("06/22/2023 18:00:00"));
        $event2->setScheduledAt(new \DateTime("06/30/2023 18:00:00"));
        $event2->setRestrictions("Phibidios menores de edad y bebidas alcoholicas.");
        $event2->setLocation("Expo Bancomer, Sanata fe ,CDMX");
        $event2->setUserId($this->getReference('user'));
        $manager->persist($event2);

        $event3 = new Event();
        $event3->setTitle("U2 Rgeresa a Mexico | ROCK POP ");
        $event3->setDescription("La enigmatica banda Irlandensa regresa a Mexico
        - Vive los 30 anios de musica rock y pop junto a la famosa banda U2");
        $event3->setAgeLimit(3);
        $event3->setBanner("http://2.bp.blogspot.com/-84Oyl0AS_Co/VMNY0EyJf_I/AAAAAAAACX8/q816NWRh838/s1600/U2.jpg");
        $event3->setEstimatedDuration("2 hora 30 minutos.");
        $event3->setOrganizer("U2 ");
        $event3->setPercentageDiscount(null);
        $event3->setPrice(3500);
        $event3->setPresale(new \DateTime("08/15/2023 15:00:00"));
        $event3->setScheduledAt(new \DateTime("08/30/2023 15:00:00"));
        $event3->setRestrictions("Limitado preventa a 4 boletos por persona.");
        $event3->setLocation("Foro Sol, CDMX");
        $event3->setUserId($this->getReference('user'));
        $manager->persist($event3);



        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }
}

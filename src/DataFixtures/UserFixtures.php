<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $admin = new User();
        $admin->setFirstName("Administrador");
        $admin->setLastName("AdminPage");
        $admin->setEmail("admin@gmail.com");
        $admin->setRoles(["ROLE_USER", "ROLE_ADMIN"]);
        $admin->setPassword("$2y$13\$uOIggzvVNd5sEmgcc7cLl.qdfPramheLVjJFiIA267RHlPxdr/bQ.");
        $manager->persist($admin);

        $user = new User();
        $user->setFirstName("Jorge");
        $user->setLastName("Regis");

        $user->setEmail("jorge@gmail.com");
        $user->setRoles(["ROLE_USER"]);
        $user->setPassword("$2y$13\$uOIggzvVNd5sEmgcc7cLl.qdfPramheLVjJFiIA267RHlPxdr/bQ.");
        $manager->persist($user);

        $manager->flush();

        $this->addReference('admin', $admin);
        $this->addReference('user', $user);
    }
}
